//slick carousel
$('.multiple-items').slick({
    draggable: true,
    pauseOnHover: true,
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 3,
    slidesToScroll: 3,
    autoplay: true,
    autoplaySpeed: 5000,
    cssEase: 'ease-in-out',
    responsive: [
        {
            breakpoint: 1199.98,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 2,
            },
        },
        {
            breakpoint: 991.98,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
            },
        },
    ],
});

//btn breakpoint: 991
let btn = document.querySelector('.m-block');
let rowNone = document.querySelector('.row_none');
btn.addEventListener('click', () => {
    rowNone.style.display = 'flex';
    btn.style.display = 'none';
});

//DARK MODE
document.querySelector('.light-mode').addEventListener('click', (event) => {
    event.preventDefault();
    if (localStorage.getItem('theme') === 'light') {
    } else {
        localStorage.setItem('theme', 'light')
    }
    addDarkClassToLight()
});
document.querySelector('.dark-mode').addEventListener('click', (event) => {
    event.preventDefault();
    if (localStorage.getItem('theme') === 'dark') {
    } else {
        localStorage.setItem('theme', 'dark')
    }
    addDarkClassToLight()
});

function addDarkClassToLight() {

    if (localStorage.getItem('theme') === 'light') {
        document.querySelector('.mode_dark_light').classList.add('light_theme');
    } else {
        document.querySelector('.mode_dark_light').classList.remove('light_theme');
    }
}

addDarkClassToLight()
